#-------------------------------------------------
#
# Project created by QtCreator 2015-05-08T10:14:41
#
#-------------------------------------------------


QT       += core
QT       += qml quick widgets

TEMPLATE = app

CONFIG   += console \
    c++11
CONFIG   -= app_bundle

SOURCES += main.cpp \
    filecompress.cpp \
    util.cpp \
    treehuffman.cpp \
    nodehuff.cpp \
    huffinfo.cpp

HEADERS += \
    filecompress.h \
    util.h \
    treehuffman.h \
    nodehuff.h \
    huffinfo.h

RESOURCES += \
    qml.qrc
