import QtQuick.Controls 1.4
import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Window{
    function mm(value){
        return value*Screen.pixelDensity
    }
    id: ui
    visible: true
    height: mm(100)
    width: mm(180)
    Row{
        Rectangle{
            id: menu
            height: ui.height
            width: ui.width*0.195
            color: "#333333"
            Column{
                spacing: 10
                anchors.centerIn: parent
                QButton{
                    txt.text: "Comprimir"
                    mouse.onClicked: {
                        _huffman.compress(choose.path,choose.pathOut)
                    }
                }
                QButton{
                    txt.text: "Descomprimir"
                    mouse.onClicked: {
                        _huffman.decompress(choose.path,choose.pathOut)
                    }
                }
            }
        }
        Rectangle{
            height: ui.height
            width: ui.width*0.005
            color:"#006e9f"
        }
        Rectangle{
            height: ui.height
            width: ui.width*0.8
            color: "#333333"
            Rectangle{
                height: ui.height/1.5
                width: height
                radius: height/2
                anchors.centerIn: parent
            }
            Rectangle{
                height: ui.height/2
                width: height
                radius: height/2
                anchors.centerIn: parent
                color:"#006e9f"
            }
            ChooseFile{
                id: choose
                anchors.centerIn: parent
            }
        }
    }
}
