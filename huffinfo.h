#pragma once

#include <QString>
#include <QBitArray>
#include <QObject>

class HuffInfo{
    int sizeTrash;
    int sizeName;
    int sizeTree;
    QBitArray code;
    QByteArray tree;
    QString name;
    QString outDir;
public:
    HuffInfo();
    HuffInfo(QString path);
    static void test();

    QString getOutDir() const;
    void setOutDir(const QString &value);
    QString getName() const;
    QByteArray getTree() const;
    QBitArray getCode() const;
    void extract(QByteArray byte);
};
