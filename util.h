#pragma once

#include <QString>
#include <QList>
#include <QHash>
#include "nodehuff.h"
#include "bitarray.h"
#include <QObject>
#include <QPair>

class Util{
public:
    static QList<NodeHuff*> toQList(int * array);
    static QList<NodeHuff*> count(QString path);
    static void toBitArray(QString codenode,BitArray *buffer);
    static bool comp(const NodeHuff* a,const NodeHuff* b);
    static int fileTemp(QString path,QHash<uchar,QString> hash);
    static bool headerTemp(QString path, QByteArray header, QString outName = "");
    static QByteArray toBinaryString(QString str);
    static QBitArray byteToBit(char byte);
    static int bitArrayToString(const QBitArray array);
    static QBitArray joinQBitArray(QBitArray a, QBitArray b);
    static QByteArray restoreData(QBitArray array, NodeHuff* root);
    static void write(QByteArray in,QString path);
    static int bitToInt(QBitArray array);
};
